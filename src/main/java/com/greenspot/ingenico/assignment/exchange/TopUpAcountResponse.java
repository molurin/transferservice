/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.greenspot.ingenico.assignment.exchange;

/**
 *
 * @author mayowa.olurin
 */
public class TopUpAcountResponse extends BaseResponse {

    private Double accountBalance;

    public Double getAccountBalance() {
        return accountBalance;
    }

    public void setAccountBalance(Double accountBalance) {
        this.accountBalance = accountBalance;
    }

    @Override
    public String toString() {
        return "TopUpAcountResponse{" + "accountBalance=" + accountBalance + '}';
    }
    
}
