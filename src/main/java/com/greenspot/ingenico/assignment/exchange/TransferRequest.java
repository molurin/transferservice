/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.greenspot.ingenico.assignment.exchange;

/**
 *
 * @author mayowa.olurin
 */
public class TransferRequest {
    
    private String senderAccountReference;
    private String recieverAccountReference;
    private Double amount;

    public String getSenderAccountReference() {
        return senderAccountReference;
    }

    public void setSenderAccountReference(String senderAccountReference) {
        this.senderAccountReference = senderAccountReference;
    }

    public String getRecieverAccountReference() {
        return recieverAccountReference;
    }

    public void setRecieverAccountReference(String recieverAccountReference) {
        this.recieverAccountReference = recieverAccountReference;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }
    
    
}
