/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.greenspot.ingenico.assignment.util;

/**
 *
 * @author mayowa.olurin
 */
public class ApiConstants {
    
    public static final String CREATE_ACCOUNT_ENDPOINT = "/create";
    public static final String GET_ACCOUNT_ENDPOINT = "{accountReference}";
    public static final String UPDATE_ACCOUNT_BALANCE_ENDPOINT = "/topup";
    
    //Transfer
    public static final String TRANSFER_MONEY_ENDPOINT = "/transfer";
    
}
