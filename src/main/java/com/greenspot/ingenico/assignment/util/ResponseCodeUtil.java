/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.greenspot.ingenico.assignment.util;

/**
 *
 * @author mayowa.olurin
 */
public class ResponseCodeUtil {
    
    public static final String SUCCESS_RESPONSE_CODE = "00";
    public static final String SUCCESS_RESPONSE_MESSAGE = "Successful";
    public static final String UNSUCCESS_RESPONSE_CODE = "1000";
    public static final String UNSUCCESS_RESPONSE_MESSAGE = "UnSuccessful";
    
    public static final String SENDER_ACCOUNT_NOT_FOUND_RESPONSE_MESSAGE = "Sender account not found.";
    public static final String RECIEVER_ACCOUNT_NOT_FOUND_RESPONSE_MESSAGE = "Reciever account not found.";
    public static final String INSUFFICIENT_FUDS_RESPONSE_MESSAGE = "Insufficient funds.";
    
}
