/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.greenspot.ingenico.assignment.util;

import java.util.Calendar;
import java.util.TimeZone;

/**
 *
 * @author mayowa.olurin
 */
public class TransferServiceUtil {

    public static long generateTimestamp() {
        Calendar calendar = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
        long epochTimeInSecs = calendar.getTimeInMillis() / 1000;
        return epochTimeInSecs;
    }

}
