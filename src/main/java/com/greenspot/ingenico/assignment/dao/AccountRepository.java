/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.greenspot.ingenico.assignment.dao;

import com.greenspot.ingenico.assignment.dao.model.Account;
import java.util.List;
import javax.persistence.LockModeType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Lock;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

/**
 *
 * @author mayowa.olurin
 */
@Repository
public interface AccountRepository extends JpaRepository<Account, Long> {

    List<Account> findByAccountReference(String accountReference);
    
    @Lock(LockModeType.PESSIMISTIC_WRITE)
    Account getByAccountReference(String accountReference);
    
    @Modifying(clearAutomatically = true)
    @Query("UPDATE Account a SET a.accountBalance = :accountBalance WHERE a.accountReference = :accountReference")
    int updateAccountBalance(@Param("accountBalance") Double accountBalance, @Param("accountReference") String accountReference);

}
