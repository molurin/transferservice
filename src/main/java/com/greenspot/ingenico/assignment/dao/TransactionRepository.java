/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.greenspot.ingenico.assignment.dao;

import com.greenspot.ingenico.assignment.dao.model.Transaction;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

/**
 *
 * @author mayowa.olurin
 */
@Repository
public interface TransactionRepository extends JpaRepository<Transaction, Long> {
    
    Transaction getByTransactionRef(String transactionRef);
    
    @Modifying(clearAutomatically = true)
    @Query("UPDATE Transaction t SET t.responseCode = :responseCode, t.responseMessage = :responseMessage WHERE t.id = :id")
    int updateTransactionResponse(@Param("responseCode") String responseCode, @Param("responseMessage") String responseMessage, @Param("id") long id);

}
