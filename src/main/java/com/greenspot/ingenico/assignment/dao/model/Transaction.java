/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.greenspot.ingenico.assignment.dao.model;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import org.hibernate.validator.constraints.NotBlank;

/**
 *
 * @author mayowa.olurin
 */
@Entity
@Table(name = "transaction")
public class Transaction extends Auditable implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    @NotBlank
    private String senderAccountReference;
    
    @NotBlank
    private String receiverAccountReference;
    
    @NotBlank
    private String transactionRef;

    private Double amount;
    
    private String responseCode;
    
    private String responseMessage;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getSenderAccountReference() {
        return senderAccountReference;
    }

    public void setSenderAccountReference(String senderAccountReference) {
        this.senderAccountReference = senderAccountReference;
    }

    public String getReceiverAccountReference() {
        return receiverAccountReference;
    }

    public void setReceiverAccountReference(String receiverAccountReference) {
        this.receiverAccountReference = receiverAccountReference;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public String getResponseCode() {
        return responseCode;
    }

    public void setResponseCode(String responseCode) {
        this.responseCode = responseCode;
    }

    public String getResponseMessage() {
        return responseMessage;
    }

    public void setResponseMessage(String responseMessage) {
        this.responseMessage = responseMessage;
    }

    public String getTransactionRef() {
        return transactionRef;
    }

    public void setTransactionRef(String transactionRef) {
        this.transactionRef = transactionRef;
    }

    
}
