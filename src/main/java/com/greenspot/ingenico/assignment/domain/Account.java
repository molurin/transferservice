/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.greenspot.ingenico.assignment.domain;

import com.greenspot.ingenico.assignment.exception.InvalidFieldException;
import jdk.nashorn.internal.ir.annotations.Immutable;

/**
 *
 * @author mayowa.olurin
 */
@Immutable
public class Account {

    private final String firstName;
    private final String lastName;
    private final String mobileNumber;
    private final String accountReference;
    private final Double accountBalance;

    private Account(Builder builder) {
        this.firstName = builder.firstName;
        this.lastName = builder.lastName;
        this.mobileNumber = builder.mobileNumber;
        this.accountReference = builder.accountReference;
        this.accountBalance = builder.accountBalance;
    }

    public static class Builder {

        private String firstName;
        private String lastName;
        private String mobileNumber;
        private String accountReference;
        private Double accountBalance;

        public Builder firstName(String firstName) {
            this.firstName = firstName;
            return this;
        }

        public Builder lastName(String lastName) {
            this.lastName = lastName;
            return this;
        }

        public Builder mobileNumber(String mobileNumber) {
            this.mobileNumber = mobileNumber;
            return this;
        }

        public Builder accountReference(String accountReference) {
            this.accountReference = accountReference;
            return this;
        }

        public Builder accountBalance(Double accountBalance) {
            this.accountBalance = accountBalance;
            return this;
        }

        public Account build() throws InvalidFieldException {

            Account account = new Account(this);
            validate(account);
            return account;
        }
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getMobileNumber() {
        return mobileNumber;
    }

    public String getAccountReference() {
        return accountReference;
    }

    public Double getAccountBalance() {
        return accountBalance;
    }

    @Override
    public String toString() {
        return "Account{" + "firstName=" + firstName + ", lastName=" + lastName + ", mobileNumber=" + mobileNumber + ", accountReference=" + accountReference + ", accountBalance=" + accountBalance + '}';
    }

    private static void validate(Account account) throws InvalidFieldException {
        if (null == account) {
            throw new InvalidFieldException("Null object");
        } else if (null == account.getAccountBalance() || account.getAccountBalance() < 0) {
            throw new InvalidFieldException("Invalid Field. Account Balance is missing");
        } else if (null == account.getAccountReference()) {
            throw new InvalidFieldException("Invalid Request. Account Reference is missing");
        } else if (null == account.getFirstName()) {
            throw new InvalidFieldException("Invalid Request. First Name is missing");
        } else if (null == account.getLastName()) {
            throw new InvalidFieldException("Invalid Request. Last Name is missing");
        } else if (null == account.getMobileNumber()) {
            throw new InvalidFieldException("Invalid Request. Mobile Number is missing");
        }
    }
}
