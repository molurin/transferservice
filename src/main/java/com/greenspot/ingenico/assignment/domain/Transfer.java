/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.greenspot.ingenico.assignment.domain;

import com.greenspot.ingenico.assignment.exception.InvalidFieldException;
import jdk.nashorn.internal.ir.annotations.Immutable;

/**
 *
 * @author mayowa.olurin
 */
@Immutable
public class Transfer {

    private final String senderAccountReference;
    private final String recieverAccountReference;
    private final Double amount;

    private Transfer(Builder builder) {
        this.senderAccountReference = builder.senderAccountReference;
        this.recieverAccountReference = builder.recieverAccountReference;
        this.amount = builder.amount;
    }

    public static class Builder {

        private String senderAccountReference;
        private String recieverAccountReference;
        private Double amount;

        public Builder senderAccountReference(String senderAccountReference) {
            this.senderAccountReference = senderAccountReference;
            return this;
        }

        public Builder recieverAccountReference(String recieverAccountReference) {
            this.recieverAccountReference = recieverAccountReference;
            return this;
        }

        public Builder amount(Double amount) {
            this.amount = amount;
            return this;
        }

        public Transfer build() throws InvalidFieldException {

            Transfer transfer = new Transfer(this);
            validate(transfer);
            return transfer;
        }
    }

    public String getSenderAccountReference() {
        return senderAccountReference;
    }

    public String getRecieverAccountReference() {
        return recieverAccountReference;
    }

    public Double getAmount() {
        return amount;
    }

    @Override
    public String toString() {
        return "Transfer{" + "senderAccountReference=" + senderAccountReference + ", recieverAccountReference=" + recieverAccountReference + ", amount=" + amount + '}';
    }

    private static void validate(Transfer transfer) throws InvalidFieldException {
        if (null == transfer) {
            throw new InvalidFieldException("Null object");
        } else if (null == transfer.getAmount() || transfer.getAmount() < 1) {
            throw new InvalidFieldException("Invalid Field. Amount is missing");
        } else if (null == transfer.getRecieverAccountReference()) {
            throw new InvalidFieldException("Invalid Request. Receiver Account Reference is missing");
        } else if (null == transfer.getSenderAccountReference()) {
            throw new InvalidFieldException("Invalid Request. Sender Reference Account is missing");
        }
    }
}
