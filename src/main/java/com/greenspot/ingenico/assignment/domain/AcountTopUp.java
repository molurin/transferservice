/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.greenspot.ingenico.assignment.domain;

import com.greenspot.ingenico.assignment.exception.InvalidFieldException;
import jdk.nashorn.internal.ir.annotations.Immutable;

/**
 *
 * @author mayowa.olurin
 */
@Immutable
public class AcountTopUp {

    private final String accountReference;
    private final Double amount;

    private AcountTopUp(Builder builder) {
        this.accountReference = builder.accountReference;
        this.amount = builder.amount;
    }

    public static class Builder {

        private String accountReference;
        private Double amount;

        public Builder accountReference(String accountReference) {
            this.accountReference = accountReference;
            return this;
        }

        public Builder amount(Double amount) {
            this.amount = amount;
            return this;
        }

        public AcountTopUp build() throws InvalidFieldException {

            AcountTopUp acountTopUp = new AcountTopUp(this);
            validate(acountTopUp);
            return acountTopUp;
        }
    }

    public String getAccountReference() {
        return accountReference;
    }

    public Double getAmount() {
        return amount;
    }

    @Override
    public String toString() {
        return "AcountTopUp{" + "accountReference=" + accountReference + ", amount=" + amount + '}';
    }

    private static void validate(AcountTopUp acountTopUp) throws InvalidFieldException {
        if (null == acountTopUp) {
            throw new InvalidFieldException("Null object");
        } else if (null == acountTopUp.getAmount() || acountTopUp.getAmount() < 1) {
            throw new InvalidFieldException("Invalid Field. Amount is missing");
        } else if (null == acountTopUp.getAccountReference()) {
            throw new InvalidFieldException("Invalid Request. Account Reference is missing");
        }
    }
}
