/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.greenspot.ingenico.assignment.exception;

/**
 *
 * @author mayowa.olurin
 */
public class InSufficientBalanceException extends Exception {

    private int code;

    /**
     *
     */
    public InSufficientBalanceException() {
        super();
    }

    /**
     * @param message
     */
    public InSufficientBalanceException(String message) {
        super(message);
    }

    public InSufficientBalanceException(int code, String locale) {
        this.code = code;
    }

    public InSufficientBalanceException(int code, String[] params, String locale) {
        this.code = code;
    }

    public InSufficientBalanceException(int code, String locale, Throwable cause) {
        this.code = code;
    }

    /**
     * @param message
     * @param cause
     */
    public InSufficientBalanceException(String message, Throwable cause) {
        super(message, cause);
    }

    /**
     * @param cause
     */
    public InSufficientBalanceException(Throwable cause) {
        super(cause);
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

}
