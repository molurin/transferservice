/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.greenspot.ingenico.assignment.exception;

/**
 *
 * @author mayowa.olurin
 */
public class AccountNotFoundException extends Exception {

    private int code;

    /**
     *
     */
    public AccountNotFoundException() {
        super();
    }

    /**
     * @param message
     */
    public AccountNotFoundException(String message) {
        super(message);
    }

    public AccountNotFoundException(int code, String locale) {
        this.code = code;
    }

    public AccountNotFoundException(int code, String[] params, String locale) {
        this.code = code;
    }

    public AccountNotFoundException(int code, String locale, Throwable cause) {
        this.code = code;
    }

    /**
     * @param message
     * @param cause
     */
    public AccountNotFoundException(String message, Throwable cause) {
        super(message, cause);
    }

    /**
     * @param cause
     */
    public AccountNotFoundException(Throwable cause) {
        super(cause);
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

}
