/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.greenspot.ingenico.assignment.controller;

import com.greenspot.ingenico.assignment.domain.Account;
import com.greenspot.ingenico.assignment.domain.AcountTopUp;
import com.greenspot.ingenico.assignment.exception.InvalidFieldException;
import com.greenspot.ingenico.assignment.exchange.CreateAccountRequest;
import com.greenspot.ingenico.assignment.exchange.CreateAccountResponse;
import com.greenspot.ingenico.assignment.exchange.GetAccountResponse;
import com.greenspot.ingenico.assignment.exchange.TopUpAcountRequest;
import com.greenspot.ingenico.assignment.exchange.TopUpAcountResponse;
import com.greenspot.ingenico.assignment.service.AccountService;
import com.greenspot.ingenico.assignment.util.ApiConstants;
import com.greenspot.ingenico.assignment.util.ResponseCodeUtil;
import java.util.Optional;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author mayowa.olurin
 */
@RestController
@RequestMapping("/api/accounts")
public class AccountController {

    private final org.slf4j.Logger logger = LoggerFactory.getLogger(getClass());

    @Autowired
    AccountService accountService;

    @RequestMapping(value = ApiConstants.CREATE_ACCOUNT_ENDPOINT, method = RequestMethod.POST)
    @ResponseBody
    public ResponseEntity<CreateAccountResponse> createAccount(@RequestBody CreateAccountRequest request) {
        CreateAccountResponse response = new CreateAccountResponse();
        HttpStatus httpStatus = HttpStatus.OK;
        try {

            Account account = new Account.Builder()
                    .accountBalance(accountService.getDefaultAccountBalance())
                    .accountReference(accountService.generateAccountRefrence())
                    .firstName(request.getFirstName())
                    .lastName(request.getLastName())
                    .mobileNumber(request.getMobileNumber())
                    .build();

            Optional<Account> createdAccount = accountService.createAccount(account);
            if (createdAccount.isPresent()) {
                response.setResponseCode(ResponseCodeUtil.SUCCESS_RESPONSE_CODE);
                response.setResponseMessage(ResponseCodeUtil.SUCCESS_RESPONSE_MESSAGE);
                response.setAccountBalance(createdAccount.get().getAccountBalance());
                response.setAccountReference(createdAccount.get().getAccountReference());
            } else {
                httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;
                response.setResponseCode(ResponseCodeUtil.UNSUCCESS_RESPONSE_CODE);
                response.setResponseMessage(ResponseCodeUtil.UNSUCCESS_RESPONSE_MESSAGE);
            }

        } catch (InvalidFieldException ex) {
            httpStatus = HttpStatus.BAD_REQUEST;
            response.setResponseCode(ResponseCodeUtil.UNSUCCESS_RESPONSE_CODE);
            response.setResponseMessage(ex.getMessage());
            logger.debug("InvalidFieldException  - " + ex.getMessage());
        } catch (Exception ex) {
            httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;
            response.setResponseCode(ResponseCodeUtil.UNSUCCESS_RESPONSE_CODE);
            response.setResponseMessage(ResponseCodeUtil.UNSUCCESS_RESPONSE_MESSAGE);
            logger.debug("Exception  - " + ex.getMessage());
        }
        return new ResponseEntity<>(response, httpStatus);
    }

    @RequestMapping(value = ApiConstants.GET_ACCOUNT_ENDPOINT, method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity<GetAccountResponse> getAccount(@PathVariable("accountReference") String accountReference) {
        GetAccountResponse response = new GetAccountResponse();
        HttpStatus httpStatus = HttpStatus.OK;
        try {

            Optional<Account> account = accountService.getAccount(accountReference);
            if (account.isPresent()) {
                response.setAccountBalance(account.get().getAccountBalance());
                response.setFirstName(account.get().getFirstName());
                response.setLastName(account.get().getLastName());
                response.setMobileNumber(account.get().getMobileNumber());
            }
            response.setResponseCode(ResponseCodeUtil.SUCCESS_RESPONSE_CODE);
            response.setResponseMessage(ResponseCodeUtil.SUCCESS_RESPONSE_MESSAGE);

        } catch (InvalidFieldException ex) {
            httpStatus = HttpStatus.BAD_REQUEST;
            response.setResponseCode(ResponseCodeUtil.UNSUCCESS_RESPONSE_CODE);
            response.setResponseMessage(ex.getMessage());
            logger.debug("InvalidFieldException - " + ex.getMessage());
        } catch (Exception ex) {
            httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;
            response.setResponseCode(ResponseCodeUtil.UNSUCCESS_RESPONSE_CODE);
            response.setResponseMessage(ResponseCodeUtil.UNSUCCESS_RESPONSE_MESSAGE);
            logger.debug("Exception  - " + ex.getMessage());
        }
        return new ResponseEntity<>(response, httpStatus);
    }

    @RequestMapping(value = ApiConstants.UPDATE_ACCOUNT_BALANCE_ENDPOINT, method = RequestMethod.POST)
    @ResponseBody
    public ResponseEntity<TopUpAcountResponse> updateAccountBalance(@RequestBody TopUpAcountRequest request) {
        TopUpAcountResponse response = new TopUpAcountResponse();
        HttpStatus httpStatus = HttpStatus.OK;
        try {

            AcountTopUp acountTopUp = new AcountTopUp.Builder()
                    .accountReference(request.getAccountReference())
                    .amount(request.getAmount())
                    .build();

            Optional<Account> account = accountService.updateAccountBalance(acountTopUp);
            if (account.isPresent()) {
                response.setAccountBalance(account.get().getAccountBalance());
                response.setResponseCode(ResponseCodeUtil.SUCCESS_RESPONSE_CODE);
                response.setResponseMessage(ResponseCodeUtil.SUCCESS_RESPONSE_MESSAGE);
            } else {
                httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;
                response.setResponseCode(ResponseCodeUtil.UNSUCCESS_RESPONSE_CODE);
                response.setResponseMessage(ResponseCodeUtil.UNSUCCESS_RESPONSE_MESSAGE);
            }

        } catch (InvalidFieldException ex) {
            httpStatus = HttpStatus.BAD_REQUEST;
            response.setResponseCode(ResponseCodeUtil.UNSUCCESS_RESPONSE_CODE);
            response.setResponseMessage(ex.getMessage());
            logger.debug("InvalidFieldException - " + ex.getMessage());
        } catch (Exception ex) {
            httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;
            response.setResponseCode(ResponseCodeUtil.UNSUCCESS_RESPONSE_CODE);
            response.setResponseMessage(ResponseCodeUtil.UNSUCCESS_RESPONSE_MESSAGE);
            logger.debug("Exception  - " + ex.getMessage());
        }
        return new ResponseEntity<>(response, httpStatus);
    }

}
