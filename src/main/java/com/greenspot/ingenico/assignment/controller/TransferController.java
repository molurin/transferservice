/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.greenspot.ingenico.assignment.controller;

import com.greenspot.ingenico.assignment.dao.model.Transaction;
import com.greenspot.ingenico.assignment.domain.Account;
import com.greenspot.ingenico.assignment.domain.Transfer;
import com.greenspot.ingenico.assignment.exception.AccountNotFoundException;
import com.greenspot.ingenico.assignment.exception.InSufficientBalanceException;
import com.greenspot.ingenico.assignment.exception.InvalidFieldException;
import com.greenspot.ingenico.assignment.exchange.TransferRequest;
import com.greenspot.ingenico.assignment.exchange.TransferResponse;
import com.greenspot.ingenico.assignment.service.AccountService;
import com.greenspot.ingenico.assignment.service.TransactionService;
import com.greenspot.ingenico.assignment.service.TransferService;
import com.greenspot.ingenico.assignment.util.ApiConstants;
import com.greenspot.ingenico.assignment.util.ResponseCodeUtil;
import java.util.Optional;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author mayowa.olurin
 */
@RestController
@RequestMapping("/api")
public class TransferController {

    private final org.slf4j.Logger logger = LoggerFactory.getLogger(getClass());

    @Autowired
    TransferService transferService;

    @Autowired
    AccountService accountService;

    @Autowired
    TransactionService transactionService;

    
    @RequestMapping(value = ApiConstants.TRANSFER_MONEY_ENDPOINT, method = RequestMethod.POST)
    @ResponseBody
    public ResponseEntity<TransferResponse> transferToAccount(@RequestBody TransferRequest request) {
        TransferResponse response = new TransferResponse();
        Transaction transaction = null;
        HttpStatus httpStatus = HttpStatus.OK;
        try {

            Transfer transfer = new Transfer.Builder()
                    .amount(request.getAmount())
                    .recieverAccountReference(request.getRecieverAccountReference())
                    .senderAccountReference(request.getSenderAccountReference())
                    .build();

            //Log transaction
            transaction = transactionService.createTransaction(transfer);
            if (null != transaction) {
                response.setTransactionRef(transaction.getTransactionRef());
                
                //Do transfer operation
                transferService.doTransfer(transfer);

                //Get sender Account balance
                Optional<Account> senderAccount = accountService.getAccount(transfer.getSenderAccountReference());
                if (senderAccount.isPresent()) {
                    response.setAccountBalance(senderAccount.get().getAccountBalance());
                }

                response.setResponseCode(ResponseCodeUtil.SUCCESS_RESPONSE_CODE);
                response.setResponseMessage(ResponseCodeUtil.SUCCESS_RESPONSE_MESSAGE);
            } else {
                httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;
                response.setResponseCode(ResponseCodeUtil.UNSUCCESS_RESPONSE_CODE);
                response.setResponseMessage(ResponseCodeUtil.UNSUCCESS_RESPONSE_MESSAGE);
            }

        } catch (InvalidFieldException ex) {
            httpStatus = HttpStatus.BAD_REQUEST;
            response.setResponseCode(ResponseCodeUtil.UNSUCCESS_RESPONSE_CODE);
            response.setResponseMessage(ex.getMessage());
            logger.debug("InvalidFieldException Error - " + ex.getMessage());
        } catch (InSufficientBalanceException ex) {
            httpStatus = HttpStatus.BAD_REQUEST;
            response.setResponseCode(ResponseCodeUtil.UNSUCCESS_RESPONSE_CODE);
            response.setResponseMessage(ex.getMessage());
            logger.debug("InSufficientBalanceException Error - " + ex.getMessage());
        } catch (AccountNotFoundException ex) {
            httpStatus = HttpStatus.BAD_REQUEST;
            response.setResponseCode(ResponseCodeUtil.UNSUCCESS_RESPONSE_CODE);
            response.setResponseMessage(ex.getMessage());
            logger.debug("AccountNotFoundException Error - " + ex.getMessage());
        } catch (Exception ex) {
            httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;
            response.setResponseCode(ResponseCodeUtil.UNSUCCESS_RESPONSE_CODE);
            response.setResponseMessage(ResponseCodeUtil.UNSUCCESS_RESPONSE_MESSAGE);
        } finally {
            //Update transaction log with response code
            if (null != transaction) {
                transactionService.updateTransaction(response.getResponseCode(), response.getResponseMessage(), transaction.getId());
            }

        }
        return new ResponseEntity<>(response, httpStatus);
    }

}
