/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.greenspot.ingenico.assignment.service.impl;

import com.greenspot.ingenico.assignment.dao.AccountRepository;
import com.greenspot.ingenico.assignment.domain.Account;
import com.greenspot.ingenico.assignment.domain.Transfer;
import com.greenspot.ingenico.assignment.exception.AccountNotFoundException;
import com.greenspot.ingenico.assignment.exception.InSufficientBalanceException;
import com.greenspot.ingenico.assignment.exception.InvalidFieldException;
import com.greenspot.ingenico.assignment.service.AccountService;
import com.greenspot.ingenico.assignment.service.TransferService;
import com.greenspot.ingenico.assignment.util.ResponseCodeUtil;
import java.util.Optional;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;

/**
 *
 * @author mayowa.olurin
 */
@Service
@Primary
public class TransferServiceImpl implements TransferService {

    @Autowired
    AccountRepository accountRepository;
    @Autowired
    AccountService accountService;

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void doTransfer(Transfer transfer) throws InvalidFieldException, InSufficientBalanceException, AccountNotFoundException {

        //Log transaction
        Optional<Account> senderAccount = accountService.fromAccountModel(accountRepository.getByAccountReference(transfer.getSenderAccountReference()));
        if (senderAccount.isPresent()) {

            //Check account balance 
            if (senderAccount.get().getAccountBalance() >= transfer.getAmount()) {

                //Debit Sender
                accountService.debitAccount(accountService.toAccountModel(senderAccount.get()), transfer.getAmount());

                Optional<Account> receiverAccount = accountService.fromAccountModel(accountRepository.getByAccountReference(transfer.getRecieverAccountReference()));
                if (receiverAccount.isPresent()) {
                    //Credit Receiver
                    accountService.creditAccount(accountService.toAccountModel(receiverAccount.get()), transfer.getAmount());

                } else {
                    throw new AccountNotFoundException(ResponseCodeUtil.RECIEVER_ACCOUNT_NOT_FOUND_RESPONSE_MESSAGE);
                }
            } else {
                throw new InSufficientBalanceException(ResponseCodeUtil.INSUFFICIENT_FUDS_RESPONSE_MESSAGE);
            }
        } else {
            throw new AccountNotFoundException(ResponseCodeUtil.SENDER_ACCOUNT_NOT_FOUND_RESPONSE_MESSAGE);
        }
    }

    /// Setter method for unit test
    @Override
    public void setAccountRepository(AccountRepository accountRepository) {
        this.accountRepository = accountRepository;
    }

    /// Setter method for unit test
    @Override
    public void setAccountService(AccountServiceImpl accountService) {
        this.accountService = accountService;
    }


}
