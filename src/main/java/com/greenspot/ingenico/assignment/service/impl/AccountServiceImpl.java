/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.greenspot.ingenico.assignment.service.impl;

import com.greenspot.ingenico.assignment.dao.AccountRepository;
import com.greenspot.ingenico.assignment.dao.model.Account;
import com.greenspot.ingenico.assignment.domain.AcountTopUp;
import com.greenspot.ingenico.assignment.exception.InvalidFieldException;
import com.greenspot.ingenico.assignment.service.AccountService;
import com.greenspot.ingenico.assignment.util.ConstantUtils;
import com.greenspot.ingenico.assignment.util.TransferServiceUtil;
import java.util.List;
import java.util.Optional;
import java.util.Random;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author mayowa.olurin
 */
@Service
@Primary
public class AccountServiceImpl implements AccountService {

    @Autowired
    AccountRepository accountRepository;

    @Override
    public Double getDefaultAccountBalance() {
        return ConstantUtils.defaultAccountBalance;
    }

    @Override
    public String generateAccountRefrence() {
        String reference = String.format("%04d", new Random().nextInt(10000));

        String timestamp = String.valueOf(TransferServiceUtil.generateTimestamp());
        reference = timestamp.substring(timestamp.length() - 9) + reference;

        return reference;
    }

    @Override
    public Account toAccountModel(com.greenspot.ingenico.assignment.domain.Account domainObject) {
        if (null == domainObject) {
            return null;
        }
        Account account = new Account();
        account.setAccountBalance(domainObject.getAccountBalance());
        account.setAccountReference(domainObject.getAccountReference());
        account.setFirstName(domainObject.getFirstName());
        account.setLastName(domainObject.getLastName());
        account.setMobileNumber(domainObject.getMobileNumber());

        return account;
    }

    @Override
    public Optional<com.greenspot.ingenico.assignment.domain.Account> fromAccountModel(Account account) throws InvalidFieldException {
        if (null == account) {
            return Optional.empty();
        }
        com.greenspot.ingenico.assignment.domain.Account domainObject = new com.greenspot.ingenico.assignment.domain.Account.Builder()
                .accountBalance(account.getAccountBalance())
                .accountReference(account.getAccountReference())
                .firstName(account.getFirstName())
                .lastName(account.getLastName())
                .mobileNumber(account.getMobileNumber())
                .build();

        return Optional.ofNullable(domainObject);
    }

    @Override
    public Optional<com.greenspot.ingenico.assignment.domain.Account> createAccount(com.greenspot.ingenico.assignment.domain.Account accountDomainObject) throws InvalidFieldException {
        return fromAccountModel(accountRepository.save(toAccountModel(accountDomainObject)));
    }

    @Override
    public Optional<com.greenspot.ingenico.assignment.domain.Account> getAccount(String accountReference) throws InvalidFieldException {
        List<Account> accounts = accountRepository.findByAccountReference(accountReference);
        if (null != accounts && accounts.size() > 0) {
            return fromAccountModel(accounts.get(0));
        }

        return Optional.empty();
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public Optional<com.greenspot.ingenico.assignment.domain.Account> updateAccountBalance(AcountTopUp acountTopUp) throws InvalidFieldException {
        Account account = accountRepository.getByAccountReference(acountTopUp.getAccountReference());
        if (null != account) {
            Double newBalance = creditAccount(account, acountTopUp.getAmount());
            account.setAccountBalance(newBalance);

            return fromAccountModel(account);
        }

        return Optional.empty();
    }

    @Override
    public Double debitAccount(Account account, Double amount) {
        Double newAmount = account.getAccountBalance() - amount;
        if (accountRepository.updateAccountBalance(newAmount, account.getAccountReference()) > 0) {
            return newAmount;
        }
        return account.getAccountBalance();// return old balance since no row was updated above
    }

    @Override
    public Double creditAccount(Account account, Double amount) {
        Double newAmount = account.getAccountBalance() + amount;
        if (accountRepository.updateAccountBalance(newAmount, account.getAccountReference()) > 0) {
            return newAmount;
        }
        return account.getAccountBalance();// return old balance since no row was updated above
    }

    /// Setter method for unit test
    @Override
    public void setAccountRepository(AccountRepository accountRepository) {
        this.accountRepository = accountRepository;
    }

}
