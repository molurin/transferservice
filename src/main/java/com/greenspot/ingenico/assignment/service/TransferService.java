/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.greenspot.ingenico.assignment.service;

import com.greenspot.ingenico.assignment.dao.AccountRepository;
import com.greenspot.ingenico.assignment.domain.Transfer;
import com.greenspot.ingenico.assignment.exception.AccountNotFoundException;
import com.greenspot.ingenico.assignment.exception.InSufficientBalanceException;
import com.greenspot.ingenico.assignment.exception.InvalidFieldException;
import com.greenspot.ingenico.assignment.service.impl.AccountServiceImpl;

/**
 *
 * @author mayowa.olurin
 */
public interface TransferService {
    
    void setAccountService(AccountServiceImpl accountService);
    
    void doTransfer(Transfer transfer) throws InvalidFieldException, InSufficientBalanceException, AccountNotFoundException;
    
    void setAccountRepository(AccountRepository accountRepository);
    
}
