/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.greenspot.ingenico.assignment.service.impl;

import com.greenspot.ingenico.assignment.dao.TransactionRepository;
import com.greenspot.ingenico.assignment.dao.model.Transaction;
import com.greenspot.ingenico.assignment.domain.Transfer;
import com.greenspot.ingenico.assignment.exception.InvalidFieldException;
import com.greenspot.ingenico.assignment.service.TransactionService;
import com.greenspot.ingenico.assignment.util.TransferServiceUtil;
import java.util.Optional;
import java.util.Random;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author mayowa.olurin
 */
@Service
@Primary
public class TransactionServiceImpl implements TransactionService {

    @Autowired
    TransactionRepository transactionRepository;

    private String generateTransactionRefrence() {
        String reference = String.format("%04d", new Random().nextInt(10000));

        String timestamp = String.valueOf(TransferServiceUtil.generateTimestamp());
        reference = "IG" + timestamp.substring(timestamp.length() - 9) + reference;

        return reference;
    }

    @Override
    public Transaction createTransaction(Transfer transfer) throws InvalidFieldException {
        Transaction transaction = new Transaction();
        transaction.setAmount(transfer.getAmount());
        transaction.setReceiverAccountReference(transfer.getRecieverAccountReference());
        transaction.setSenderAccountReference(transfer.getSenderAccountReference());
        transaction.setTransactionRef(generateTransactionRefrence());

        return transactionRepository.save(transaction);
    }

    @Override
    public Optional<Transaction> getTransaction(String transactionref) {
        Transaction transaction = transactionRepository.getByTransactionRef(transactionref);
        if (null != transaction) {
            return Optional.ofNullable(transaction);
        }

        return Optional.empty();
    }

    @Transactional
    @Override
    public void updateTransaction(String responseCode, String responseMessage, long transactionId) {
        transactionRepository.updateTransactionResponse(responseCode, responseMessage, transactionId);
    }

    /// Setter method for unit test
    @Override
    public void setTransactionRepository(TransactionRepository transactionRepository) {
        this.transactionRepository = transactionRepository;
    }

}
