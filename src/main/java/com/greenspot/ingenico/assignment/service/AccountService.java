/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.greenspot.ingenico.assignment.service;

import com.greenspot.ingenico.assignment.dao.AccountRepository;
import com.greenspot.ingenico.assignment.dao.model.Account;
import com.greenspot.ingenico.assignment.domain.AcountTopUp;
import com.greenspot.ingenico.assignment.exception.InvalidFieldException;
import java.util.Optional;

/**
 *
 * @author mayowa.olurin
 */
public interface AccountService {
    
    void setAccountRepository(AccountRepository accountRepository);
    
     Account toAccountModel(com.greenspot.ingenico.assignment.domain.Account domainObject);
     
     Optional<com.greenspot.ingenico.assignment.domain.Account> fromAccountModel(Account account) throws InvalidFieldException;
     
     Optional<com.greenspot.ingenico.assignment.domain.Account> createAccount(com.greenspot.ingenico.assignment.domain.Account accountDomainObject) throws InvalidFieldException;
     
     Optional<com.greenspot.ingenico.assignment.domain.Account> getAccount(String accountReference) throws InvalidFieldException;
     
     Optional<com.greenspot.ingenico.assignment.domain.Account> updateAccountBalance(AcountTopUp acountTopUp) throws InvalidFieldException;
     
     Double debitAccount(Account account, Double amount);
     
     Double creditAccount(Account account, Double amount);
     
     String generateAccountRefrence();
     
     Double getDefaultAccountBalance();
    
}
