/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.greenspot.ingenico.assignment.service;

import com.greenspot.ingenico.assignment.dao.TransactionRepository;
import com.greenspot.ingenico.assignment.dao.model.Transaction;
import com.greenspot.ingenico.assignment.domain.Transfer;
import com.greenspot.ingenico.assignment.exception.InvalidFieldException;
import java.util.Optional;

/**
 *
 * @author mayowa.olurin
 */
public interface TransactionService {

    Transaction createTransaction(Transfer transfer) throws InvalidFieldException;

    Optional<Transaction> getTransaction(String transactionref);

    void setTransactionRepository(TransactionRepository transactionRepository);

    void updateTransaction(String responseCode, String responseMessage, long transactionId);

}
