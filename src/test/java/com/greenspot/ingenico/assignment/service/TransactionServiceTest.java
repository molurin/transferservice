/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.greenspot.ingenico.assignment.service;

import com.greenspot.ingenico.assignment.service.impl.TransactionServiceImpl;
import com.greenspot.ingenico.assignment.dao.TransactionRepository;
import com.greenspot.ingenico.assignment.dao.model.Transaction;
import com.greenspot.ingenico.assignment.domain.Transfer;
import com.greenspot.ingenico.assignment.exception.InvalidFieldException;
import java.util.logging.Level;
import java.util.logging.Logger;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import org.junit.Before;
import org.junit.Test;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 *
 * @author mayowa.olurin
 */
public class TransactionServiceTest {

    private static final Double DAFAULT_ACCOUNT_BALANCE = 10.0;
    private static final String ACCOUNT_REFRENCE = "100002";
    private static final String TRANSACTION_REFRENCE = "IG1000020909";

    private TransactionService transactionService;
    private TransactionRepository transactionRepository;

    @Before
    public void setUp() {
        transactionService = new TransactionServiceImpl();
        transactionRepository = mock(TransactionRepository.class);
        transactionService.setTransactionRepository(transactionRepository);
    }

    @Test
    public void createTransactionTest() {
        try {
            Transfer transferObject = generateTransferDomainObject();

            Transaction expectedTransaction = getTransactionModel(transferObject);
            when(transactionRepository.save(any(Transaction.class))).thenReturn(expectedTransaction);

            Transaction returned = transactionService.createTransaction(transferObject);

            assertEquals(transferObject.getAmount(), returned.getAmount());
            assertEquals(transferObject.getRecieverAccountReference(), returned.getReceiverAccountReference());
            assertEquals(transferObject.getSenderAccountReference(), returned.getSenderAccountReference());
            assertEquals(TRANSACTION_REFRENCE, returned.getTransactionRef());
        } catch (InvalidFieldException ex) {
            Logger.getLogger(AccountServiceTest.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Test
    public void getTransactionTest() {
        try {
            Transfer transferObject = generateTransferDomainObject();
            Transaction expectedTransaction = getTransactionModel(transferObject);

            when(transactionRepository.getByTransactionRef(any(String.class))).thenReturn(expectedTransaction);

            Transaction returned = transactionService.getTransaction(TRANSACTION_REFRENCE).orElse(null);

            assertNotNull(returned);
            assertEquals(transferObject.getAmount(), returned.getAmount());
            assertEquals(transferObject.getRecieverAccountReference(), returned.getReceiverAccountReference());
            assertEquals(transferObject.getSenderAccountReference(), returned.getSenderAccountReference());
            assertEquals(TRANSACTION_REFRENCE, returned.getTransactionRef());
        } catch (InvalidFieldException ex) {
            Logger.getLogger(AccountServiceTest.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private Transaction getTransactionModel(Transfer transferObject) {
        Transaction transaction = new Transaction();
        transaction.setAmount(transferObject.getAmount());
        transaction.setReceiverAccountReference(transferObject.getRecieverAccountReference());
        transaction.setSenderAccountReference(transferObject.getSenderAccountReference());
        transaction.setTransactionRef(TRANSACTION_REFRENCE);

        return transaction;
    }

    private com.greenspot.ingenico.assignment.domain.Transfer generateTransferDomainObject() throws InvalidFieldException {
        return new com.greenspot.ingenico.assignment.domain.Transfer.Builder()
                .amount(DAFAULT_ACCOUNT_BALANCE)
                .recieverAccountReference(ACCOUNT_REFRENCE)
                .senderAccountReference(ACCOUNT_REFRENCE)
                .build();
    }

}
