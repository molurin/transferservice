/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.greenspot.ingenico.assignment.service;

import com.greenspot.ingenico.assignment.service.impl.AccountServiceImpl;
import com.greenspot.ingenico.assignment.service.impl.TransferServiceImpl;
import com.greenspot.ingenico.assignment.dao.AccountRepository;
import com.greenspot.ingenico.assignment.dao.model.Account;
import com.greenspot.ingenico.assignment.exception.AccountNotFoundException;
import com.greenspot.ingenico.assignment.exception.InSufficientBalanceException;
import com.greenspot.ingenico.assignment.exception.InvalidFieldException;
import java.util.Optional;
import java.util.logging.Level;
import java.util.logging.Logger;
import static org.junit.Assert.assertEquals;
import org.junit.Before;
import org.junit.Test;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 *
 * @author mayowa.olurin
 */
public class TransferServiceTest {

    private static final Double DAFAULT_ACCOUNT_BALANCE = 10.0;
    private static final String ACCOUNT_BALANCE_REFRENCE = "100002";
    private static final String FIRST_NAME = "Mayowa";
    private static final String LAST_NAME = "Olurin";
    private static final String MOBILE_NUMBER = "0800000002";

    private AccountServiceImpl accountServiceMock;
    private TransferServiceImpl transferService;
    private AccountRepository accountRepositoryMock;

    @Before
    public void setUp() {
        transferService = new TransferServiceImpl();
        accountRepositoryMock = mock(AccountRepository.class);
        accountServiceMock = mock(AccountServiceImpl.class);
        transferService.setAccountRepository(accountRepositoryMock);
        transferService.setAccountService(accountServiceMock);
    }

    @Test
    public void doTransferTest() {
        try {
            com.greenspot.ingenico.assignment.domain.Transfer transferObject = generateTransferDomainObject();
            com.greenspot.ingenico.assignment.domain.Account accountObject = generateAccountDomainObject();

            when(accountServiceMock.fromAccountModel(any(Account.class))).thenReturn(Optional.ofNullable(accountObject));
            when(accountServiceMock.debitAccount(any(Account.class), any(Double.class))).thenReturn(DAFAULT_ACCOUNT_BALANCE);
            when(accountServiceMock.creditAccount(any(Account.class), any(Double.class))).thenReturn(DAFAULT_ACCOUNT_BALANCE);

            transferService.doTransfer(transferObject);

            when(accountServiceMock.getAccount(any(String.class))).thenReturn(Optional.ofNullable(accountObject));

            com.greenspot.ingenico.assignment.domain.Account returned = accountServiceMock.getAccount(ACCOUNT_BALANCE_REFRENCE).get();

            assertEquals(DAFAULT_ACCOUNT_BALANCE, returned.getAccountBalance(), 0.0);

        } catch (InvalidFieldException ex) {
            Logger.getLogger(AccountServiceTest.class.getName()).log(Level.SEVERE, null, ex);
        } catch (InSufficientBalanceException | AccountNotFoundException ex) {
            Logger.getLogger(TransferServiceTest.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Test(expected = AccountNotFoundException.class)
    public void doTransferAccountNotFoundTest() throws AccountNotFoundException {
        try {
            com.greenspot.ingenico.assignment.domain.Transfer transferObject = generateTransferDomainObject();

            when(accountServiceMock.fromAccountModel(any(Account.class))).thenReturn(Optional.empty());
            transferService.doTransfer(transferObject);

        } catch (InvalidFieldException ex) {
            Logger.getLogger(AccountServiceTest.class.getName()).log(Level.SEVERE, null, ex);
        } catch (InSufficientBalanceException ex) {
            Logger.getLogger(TransferServiceTest.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Test(expected = InSufficientBalanceException.class)
    public void doTransferINsufficientBalanceTest() throws InSufficientBalanceException {
        try {
            com.greenspot.ingenico.assignment.domain.Transfer transferObject = generateTransferDomainObject();
            com.greenspot.ingenico.assignment.domain.Account accountObject = generateBrokeAccountDomainObject();

            when(accountServiceMock.fromAccountModel(any(Account.class))).thenReturn(Optional.ofNullable(accountObject));
            transferService.doTransfer(transferObject);

        } catch (InvalidFieldException ex) {
            Logger.getLogger(AccountServiceTest.class.getName()).log(Level.SEVERE, null, ex);
        } catch (AccountNotFoundException ex) {
            Logger.getLogger(TransferServiceTest.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private com.greenspot.ingenico.assignment.domain.Account generateAccountDomainObject() throws InvalidFieldException {
        return new com.greenspot.ingenico.assignment.domain.Account.Builder()
                .accountBalance(DAFAULT_ACCOUNT_BALANCE)
                .accountReference(ACCOUNT_BALANCE_REFRENCE)
                .firstName(FIRST_NAME)
                .lastName(LAST_NAME)
                .mobileNumber(MOBILE_NUMBER)
                .build();
    }

    private com.greenspot.ingenico.assignment.domain.Account generateBrokeAccountDomainObject() throws InvalidFieldException {
        return new com.greenspot.ingenico.assignment.domain.Account.Builder()
                .accountBalance(5.0)
                .accountReference(ACCOUNT_BALANCE_REFRENCE)
                .firstName(FIRST_NAME)
                .lastName(LAST_NAME)
                .mobileNumber(MOBILE_NUMBER)
                .build();
    }

    private com.greenspot.ingenico.assignment.domain.Transfer generateTransferDomainObject() throws InvalidFieldException {
        return new com.greenspot.ingenico.assignment.domain.Transfer.Builder()
                .amount(DAFAULT_ACCOUNT_BALANCE)
                .recieverAccountReference(ACCOUNT_BALANCE_REFRENCE)
                .senderAccountReference(ACCOUNT_BALANCE_REFRENCE)
                .build();
    }

}
