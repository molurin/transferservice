/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.greenspot.ingenico.assignment.service;

import com.greenspot.ingenico.assignment.service.impl.AccountServiceImpl;
import com.greenspot.ingenico.assignment.dao.AccountRepository;
import com.greenspot.ingenico.assignment.dao.model.Account;
import com.greenspot.ingenico.assignment.domain.AcountTopUp;
import com.greenspot.ingenico.assignment.exception.InvalidFieldException;
import com.greenspot.ingenico.assignment.util.ConstantUtils;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import org.junit.Before;
import org.junit.Test;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 *
 * @author mayowa.olurin
 */
public class AccountServiceTest {

    private static final Double DAFAULT_ACCOUNT_BALANCE = 10.0;
    private static final String ACCOUNT_BALANCE_REFRENCE = "100002";
    private static final String FIRST_NAME = "Mayowa";
    private static final String LAST_NAME = "Olurin";
    private static final String MOBILE_NUMBER = "0800000002";

    private AccountService accountService;
    private AccountRepository accountRepositoryMock;

    @Before
    public void setUp() {
        accountService = new AccountServiceImpl();
        accountRepositoryMock = mock(AccountRepository.class);
        accountService.setAccountRepository(accountRepositoryMock);
    }

    @Test
    public void createAccountTest() {
        try {
            com.greenspot.ingenico.assignment.domain.Account accountDomainObject = generateAccountDomainObject();

            Account savedAccount = getSampleAccountModel();
            when(accountRepositoryMock.save(any(Account.class))).thenReturn(savedAccount);

            com.greenspot.ingenico.assignment.domain.Account returned = accountService.createAccount(accountDomainObject).get();

            assertEquals(accountDomainObject.getAccountBalance(), returned.getAccountBalance());
            assertEquals(accountDomainObject.getAccountReference(), returned.getAccountReference());
            assertEquals(accountDomainObject.getFirstName(), returned.getFirstName());
            assertEquals(accountDomainObject.getLastName(), returned.getLastName());
            assertEquals(accountDomainObject.getMobileNumber(), returned.getMobileNumber());
        } catch (InvalidFieldException ex) {
            Logger.getLogger(AccountServiceTest.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Test
    public void getAccountTest() {
        try {
            com.greenspot.ingenico.assignment.domain.Account expectedDomainObject = generateAccountDomainObject();

            Account account = getSampleAccountModel();
            List<Account> accounts = new ArrayList<>();
            accounts.add(account);
            when(accountRepositoryMock.findByAccountReference(any(String.class))).thenReturn(accounts);

            com.greenspot.ingenico.assignment.domain.Account returned = accountService.getAccount(ACCOUNT_BALANCE_REFRENCE).get();

            assertEquals(expectedDomainObject.getAccountBalance(), returned.getAccountBalance());
            assertEquals(expectedDomainObject.getAccountReference(), returned.getAccountReference());
            assertEquals(expectedDomainObject.getFirstName(), returned.getFirstName());
            assertEquals(expectedDomainObject.getLastName(), returned.getLastName());
            assertEquals(expectedDomainObject.getMobileNumber(), returned.getMobileNumber());
        } catch (InvalidFieldException ex) {
            Logger.getLogger(AccountServiceTest.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Test
    public void updateAccountBalanceTest() {
        try {

            com.greenspot.ingenico.assignment.domain.Account expectedDomainObject = generateAccountDomainObject();

            com.greenspot.ingenico.assignment.domain.AcountTopUp topUp = new AcountTopUp.Builder()
                    .accountReference(ACCOUNT_BALANCE_REFRENCE)
                    .amount(DAFAULT_ACCOUNT_BALANCE)
                    .build();

            Account account = getSampleAccountModel();
            Double newAmount = account.getAccountBalance() + topUp.getAmount();
            when(accountRepositoryMock.getByAccountReference(any(String.class))).thenReturn(account);
            when(accountRepositoryMock.updateAccountBalance(any(Double.class), any(String.class))).thenReturn(1);

            com.greenspot.ingenico.assignment.domain.Account returned = accountService.updateAccountBalance(topUp).get();

            assertNotEquals(expectedDomainObject.getAccountBalance(), returned.getAccountBalance());
            assertEquals(newAmount, returned.getAccountBalance());
            assertEquals(expectedDomainObject.getAccountReference(), returned.getAccountReference());
            assertEquals(expectedDomainObject.getFirstName(), returned.getFirstName());
            assertEquals(expectedDomainObject.getLastName(), returned.getLastName());
            assertEquals(expectedDomainObject.getMobileNumber(), returned.getMobileNumber());
        } catch (InvalidFieldException ex) {
            Logger.getLogger(AccountServiceTest.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Test
    public void debitAccountTest() {
        try {

            Account account = getSampleAccountModel();
            Double expectedAmount = account.getAccountBalance() - DAFAULT_ACCOUNT_BALANCE;
            when(accountRepositoryMock.updateAccountBalance(any(Double.class), any(String.class))).thenReturn(1);

            Double returned = accountService.debitAccount(account, DAFAULT_ACCOUNT_BALANCE);

            assertNotEquals(expectedAmount, account.getAccountBalance());
            assertEquals(expectedAmount, returned);
        } catch (Exception ex) {
            Logger.getLogger(AccountServiceTest.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Test
    public void debitAccountNegativeTest() {
        try {

            Account account = getSampleAccountModel();
            Double expectedAmount = account.getAccountBalance() - DAFAULT_ACCOUNT_BALANCE;
            when(accountRepositoryMock.updateAccountBalance(any(Double.class), any(String.class))).thenReturn(0);

            Double returned = accountService.debitAccount(account, DAFAULT_ACCOUNT_BALANCE);

            assertNotEquals(expectedAmount, returned);
            assertEquals(account.getAccountBalance(), returned);
        } catch (Exception ex) {
            Logger.getLogger(AccountServiceTest.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Test
    public void creditAccountTest() {
        try {

            Account account = getSampleAccountModel();
            Double expectedAmount = account.getAccountBalance() + DAFAULT_ACCOUNT_BALANCE;
            when(accountRepositoryMock.updateAccountBalance(any(Double.class), any(String.class))).thenReturn(1);

            Double returned = accountService.creditAccount(account, DAFAULT_ACCOUNT_BALANCE);

            assertNotEquals(expectedAmount, account.getAccountBalance());
            assertEquals(expectedAmount, returned);
        } catch (Exception ex) {
            Logger.getLogger(AccountServiceTest.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Test
    public void creditAccountNegativeTest() {
        try {

            Account account = getSampleAccountModel();
            Double expectedAmount = account.getAccountBalance() + DAFAULT_ACCOUNT_BALANCE;
            when(accountRepositoryMock.updateAccountBalance(any(Double.class), any(String.class))).thenReturn(0);

            Double returned = accountService.creditAccount(account, DAFAULT_ACCOUNT_BALANCE);

            assertNotEquals(expectedAmount, returned);
            assertEquals(account.getAccountBalance(), returned);
        } catch (Exception ex) {
            Logger.getLogger(AccountServiceTest.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Test
    public void toAccountModelTest() {
        try {
            com.greenspot.ingenico.assignment.domain.Account expectedDomainObject = generateAccountDomainObject();

            Account returned = accountService.toAccountModel(expectedDomainObject);

            assertEquals(expectedDomainObject.getAccountBalance(), returned.getAccountBalance());
            assertEquals(expectedDomainObject.getAccountReference(), returned.getAccountReference());
            assertEquals(expectedDomainObject.getFirstName(), returned.getFirstName());
            assertEquals(expectedDomainObject.getLastName(), returned.getLastName());
            assertEquals(expectedDomainObject.getMobileNumber(), returned.getMobileNumber());
        } catch (InvalidFieldException ex) {
            Logger.getLogger(AccountServiceTest.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Test
    public void toAccountModelNegativeTest() {
        try {

            assertNull(accountService.toAccountModel(null));
        } catch (Exception ex) {
            Logger.getLogger(AccountServiceTest.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Test
    public void fromAccountModelTest() {
        try {
            com.greenspot.ingenico.assignment.domain.Account expectedDomainObject = generateAccountDomainObject();
            Account account = getSampleAccountModel();

            com.greenspot.ingenico.assignment.domain.Account returned = accountService.fromAccountModel(account).get();

            assertEquals(expectedDomainObject.getAccountBalance(), returned.getAccountBalance());
            assertEquals(expectedDomainObject.getAccountReference(), returned.getAccountReference());
            assertEquals(expectedDomainObject.getFirstName(), returned.getFirstName());
            assertEquals(expectedDomainObject.getLastName(), returned.getLastName());
            assertEquals(expectedDomainObject.getMobileNumber(), returned.getMobileNumber());
        } catch (InvalidFieldException ex) {
            Logger.getLogger(AccountServiceTest.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Test
    public void fromAccountModelNegativeTest() {
        try {

            assertFalse(accountService.fromAccountModel(null).isPresent());
        } catch (InvalidFieldException ex) {
            Logger.getLogger(AccountServiceTest.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Test
    public void generateAccountRefrenceTest() {
        try {
            String ref = accountService.generateAccountRefrence();

            assertNotNull(ref);
            assertEquals(13, ref.length());
        } catch (Exception ex) {
            Logger.getLogger(AccountServiceTest.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Test
    public void getDefaultAccountBalanceTest() {
        try {
            Double defaultBalance = accountService.getDefaultAccountBalance();
            assertNotNull(defaultBalance);
            assertEquals(ConstantUtils.defaultAccountBalance, defaultBalance, 0.0);
        } catch (Exception ex) {
            Logger.getLogger(AccountServiceTest.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private com.greenspot.ingenico.assignment.domain.Account generateAccountDomainObject() throws InvalidFieldException {
        return new com.greenspot.ingenico.assignment.domain.Account.Builder()
                .accountBalance(DAFAULT_ACCOUNT_BALANCE)
                .accountReference(ACCOUNT_BALANCE_REFRENCE)
                .firstName(FIRST_NAME)
                .lastName(LAST_NAME)
                .mobileNumber(MOBILE_NUMBER)
                .build();
    }

    private Account getSampleAccountModel() {
        Account savedAccount = new Account();
        savedAccount.setAccountBalance(DAFAULT_ACCOUNT_BALANCE);
        savedAccount.setAccountReference(ACCOUNT_BALANCE_REFRENCE);
        savedAccount.setFirstName(FIRST_NAME);
        savedAccount.setLastName(LAST_NAME);
        savedAccount.setMobileNumber(MOBILE_NUMBER);

        return savedAccount;
    }

}
