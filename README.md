
# Transfer Service

Service for transfer of funds from one account to the other

## Solution Design

### Technology Used

Technologies used are

#### Maven
Maven was used to manage dependencies

#### Spring Boot
Spring boot was chosen to speed up development by providing automatic  Spring configurations and simple Maven Configurations. Spring boot also allows us to create stand-alone spring application with embedded Tomcat, therefore simplifying installation and enviroment dependencies. 

#### Spring Data + JPA + Hibernate
Spring data JPA provides CRUD capabilities to our model objects without the need of any boilerplate code. It abstracts datasource implementaions for us and make it easy to change datasource without changing code.

#### MYSQL

## Simple Design
Controller > Service > DAO


## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes.

### Prerequisites

A running MySQL is needed to run the application. Create a database and name it 'assignment'.

### Installing

Simply build project and find jar executable in /target. Just run jar

java -jar TransferService-0.0.1-SNAPSHOT.jar

You can also install as a init.d service

$ sudo ln -s /var/myapp/TransferService-0.0.1-SNAPSHOT.jar /etc/init.d/transferservice

$ service transferservice start


## REST API 

### Authentication - JWT

To call API you need to login and get JWT token back in header

POST http://localhost:8080/token
{"username":"admin","password":"password"}

### Transfer Funds

POST http://localhost:8080/api/transfer
{"senderAccountReference":"5141142596396","recieverAccountReference":"5141142720015", "amount":10}


### Create Account

POST http://localhost:8080/api/accounts/create
{"firstName":"Mayowa","lastName":"Olurin", "mobileNumber":"08053343434"}

### Fund Account

POST http://localhost:8080/api/accounts/topup
{"accountReference":"5141142720015", "amount":10}







